
function MyArray () {
  let counter = 0;
  for (let el in arguments) {
    this[counter] = arguments[counter];
    counter++
  };
	this.length = counter;
	this[Symbol.iterator] = function () {
    const end = this.length
    let current = 0
    return {
      next: () => {
        if (current < end) {
          return {done: false, value: this[current++]}
        }
        return {done: true}
      }
    }
  }
};

MyArray.prototype.push = function() {

  if (arguments) {
    for (let el of arguments) {
      let index = this.length;
      this[index] = el;
			this.length++;
		}
	}
};

MyArray.prototype.pop = function() {
  let index = this.length - 1;
  delete this[index];
  this.length = index;
};

MyArray.from = function (arrayLike) {
  const newArr = new MyArray();
  for (let i = 0; i < arrayLike.length; i++) {
    newArr.push(arrayLike[i]);
  }
  return newArr;
};

MyArray.prototype.map = function (callback) {
  let newArr = new MyArray();
  for (let i = 0; i < this.length; i++) { 
    newArr[i] = callback(this[i], i, this);
  };
  return newArr;
};


MyArray.prototype.forEach = function (callback) {
  for (let i = 0; i < this.length; i++) { 
    this[i] = callback(this[i], i, this);
  }
};

MyArray.prototype.reduce = function(callback, initialValue = 0) {
  for (let i = 0; i < this.length; i++) {
    initialValue = callback(initialValue, this[i], i, this);
  }
  return initialValue;
};

MyArray.prototype.filter = function(callback) {
  let newArr = new MyArray();
  for (let i = 0; i < this.length; i++) {
    if (callback(this[i], i, this)) {
      newArr.push(this[i]);
    }
  }
  return newArr;
};

MyArray.prototype.sort = function(compareFunction) {
	for (let i = 0; i < this.length; i++) {
		for (let j = 0; j < this.length; j++) {
			if (compareFunction(this[j], this[j + 1]) > 0) {
				let swap =  this[j];
				this[j] = this[j + 1];
				this[j + 1] = swap;
			}
		}
	}
	return this
};

MyArray.prototype.toString = function () {
	let string = '';
	for (let i = 0; i < this.length; i++) {
		if (i !== (this.length - 1)) {
			string += `${this[i]},`;
		} else {
				string += this[i];
			}
	}
	return string;
}	

